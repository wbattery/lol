local Meta = {Name = "wbBrand", Version = "1.0.0"}

local Player = _G.Player

if Player.CharName ~= "Brand" then return end

local clean = _G.clean

module(Meta.Name, package.seeall, log.setup)
clean.module(Meta.Name, package.seeall, log.setup)
--_G.CoreEx.AutoUpdate("https://bitbucket.org/wbattery/lol/raw/master/WBBrand.lua", "1.0.0")


local CoreEx = _G.CoreEx
local Libs = _G.Libs

local Menu = Libs.NewMenu

local Game = CoreEx.Game
local Input = CoreEx.Input
local Enums = CoreEx.Enums
local Renderer = CoreEx.Renderer
local ObjectManager = CoreEx.ObjectManager
local EventManager = CoreEx.EventManager
local Vector = CoreEx.Geometry.Vector
local Circle = CoreEx.Geometry.Circle
local Rectangle = CoreEx.Geometry.Rectangle
local BestCoveringCircle = CoreEx.Geometry.BestCoveringCircle

local TargetSelector = Libs.TargetSelector
local TS = Libs.TargetSelector
local Spell = Libs.Spell
local Orbwalker = Libs.Orbwalker
local DamageLib = Libs.DamageLib
local HPred = Libs.HealthPred
local Prediction = Libs.Prediction
local CollisionLib = Libs.CollisionLib

local next = _G.next
local sort = _G.table.sort

local Utils = {}

function Utils.map(tbl, f)
    local t = {}
    for k,v in pairs(tbl) do
        t[k] = f(v)
    end
    return t
end

function Utils.len(T)
    local count = 0
    for _ in pairs(T) do count = count + 1 end
    return count
end

function Utils.filter(tbl, f)
    local t = {}
    local n = 1
    for _, v in pairs(tbl) do
        if f(v) then
            t[n] = v
            n = n + 1
        end
    end
    return t
end

function Utils.IsValidTarget(target)
    return target and target.IsTargetable and target.IsAlive
end

function Utils.ValidMinion(minion)
	return minion and minion.IsTargetable and minion.MaxHealth > 6 -- check if not plant or shroom
end

-- Table
function table.contains(tbl, e)
    for _, v in pairs(tbl) do
        if v == e then
            return true
        end
    end

    return false
end

function table.copy(tbl)
    local t = {}

    for _, v in pairs(tbl) do
        table.insert(t, v)
    end

    return t
end


function table.len(tbl)
    local count = 0
    for _ in pairs(tbl) do count = count + 1 end
    return count
end


-- BFS
function Utils.bfs(start, graph, goal)
    if not graph[start] then
        return false
    end

    local visited = {}
    local queue = Utils.NewQueue()

    queue:push({start})
    table.insert(visited, start)

    while queue:count() > 0 do
        local path = queue:pull()
        local node = path[#path]

        if node == goal then return path end

        for _, exit in pairs(graph[node]) do
            if not table.contains(visited, exit) then
                table.insert(visited, exit)

                if graph[exit] then
                    local new = table.copy(path)

                    table.insert(new, exit)
                    queue:push(new)
                end
            end
        end
    end

    return false
end

local Spells = {
    Q =  {
        Slot = Enums.SpellSlots.Q,
        SlotString = "Q",
        Range = 1040,
        Width = 120,
        Radius = 120/2,
        Speed = 1600,
        Delay = 0.25,
        UseHitbox = true,
        Type = "Linear",
        Collisions={Heroes=false, Minions=true, WindWall=true, Wall=false}
    },
    W = {
        Slot = Enums.SpellSlots.W,
        SlotString = "W",
        Range = 900,
        Radius = 260,
        EffectRadius = 260,
        Delay = 0.25,
        Type = "Circular"
    },
    E = {
        Slot = Enums.SpellSlots.E,
        SlotString = "E",
        Range = 675,
        EffectRadius = 300,
        Delay = 0.25,
        Type = "Circular"
    },
    E2 = {
        Slot = Enums.SpellSlots.E,
        SlotString = "E",
        Range = 675,
        EffectRadius = 600,
        Delay = 0.25,
        Type = "Circular"
    },
    R = {
        Slot = Enums.SpellSlots.R,
        SlotString = "R",
        Range = 750,
        EffectRadius = 600,
        Delay = 0.25
    }
}

local WBBrand = {
    NextAuto = 0,
    LastTick = 0,
}

function WBBrand.AutoAttack(target)
    if target then
        if WBBrand.NextAuto < Game.GetTime() then
            if Input.Attack(target) then
                WBBrand.NextAuto = Game.GetTime() + Player.AttackDelay
                return true
            end
        end
    end
end

function WBBrand.IsAblaze(target) 
    if target:GetBuff("BrandAblaze") then
        return true
    end
    return false
end

function WBBrand.predQDelay()
    return Spells.Q.Delay - (Game.GetLatency()/1000)
end

function WBBrand.predWDelay()
    return Menu.Get("PredDelayW") - (Game.GetLatency()/1000)
end

function WBBrand.predEDelay()
    return Spells.E.Delay - (Game.GetLatency()/1000)
end

function WBBrand.predRDelay()
    return Spells.R.Delay - (Game.GetLatency()/1000)
end

function WBBrand.GetTarget(range)
    local target = Orbwalker.GetTarget() 
    local enemies = ObjectManager.GetNearby("enemy", "heroes")
    local enemiesInRange = Utils.filter(enemies, function (enemy)
        return Player.Position:Distance(enemy) < range and Utils.IsValidTarget(enemy)
    end)
    sort(enemiesInRange, function (a, b)
        return a.Health < b.Health
    end)
    if Utils.len(enemiesInRange) == 0 then
        return target
    elseif target and target.Health < enemiesInRange[1].Health then
        return enemiesInRange[1]
    else
        return enemiesInRange[1]
    end
end

function WBBrand.FindConflagTarget()
    -- Simple case, enemy is in range
    local enemies = ObjectManager.Get("enemy", "heroes")
    local enemyPos = {}
    sort(enemies, function (a, b)
        return a.Health < b.Health
    end)

    if table.len(enemies) > 0 then
        for _, v in pairs(enemies) do
            enemyPos[v] = v:FastPrediction(WBBrand.predEDelay())

            if Player.Position:Distance(enemyPos[v]) < Spells.E.Range and Utils.IsValidTarget(v) and WBBrand.IsAblaze(v) then
                return v
            end
        end
    end

    -- queue
    local queue = {}
    queue.stack = {}
    queue.c = 0
    function queue.push(e)
        queue.c = queue.c + 1
        table.insert(queue.stack, e)
    end
    function queue:pull()
        local e = queue.stack[1]
        table.remove(queue.stack, 1)

        queue.c = queue.c - 1
        return e
    end
    function queue:count()
        return queue.c
    end

    -- Complex, minion is in range
    local visited = {}
    local path = {}
    for _, v in pairs(ObjectManager.GetNearby("enemy", "minions")) do
        if Player.Position:Distance(v) < Spells.E.Range and Utils.IsValidTarget(v) then
            queue.push(v)
            path[v] = 0
        end
    end

    function path.GetRoot(v)
        local root = path[v]
        if root == 0 or root == nil then
            return v
        end
        return path.GetRoot(root)
    end

    while queue.count() > 0 do
        local node = queue.pull()
        if node and not table.contains(visited, node) then
            -- check if this node reaches a hero or not
            local range = Spells.E.EffectRadius
            if WBBrand.IsAblaze(node) then
                range = Spells.E.EffectRadius * 2
            end
            for _, v in pairs(enemies) do
                if node.Position:Distance(enemyPos[v]) < range and Utils.IsValidTarget(v) and WBBrand.IsAblaze(v) then
                    return path.GetRoot(node)
                end
            end
            -- if not add nearby minions
            for _, v in pairs(ObjectManager.Get("enemy", "minions")) do
                if node.Position:Distance(v) < range and Utils.IsValidTarget(v) then
                    local parent = path[v]
                    if parent == nil then
                        queue.push(v)
                        path[v] = node
                    end
                end
            end
        end
        table.insert(visited, node)
    end
    if table.len(enemies) > 0 then
        for _, v in pairs(enemies) do
            if Player.Position:Distance(v) < Spells.E.Range and Utils.IsValidTarget(v) then
                return v
            end
        end
    end
    return nil
end

function WBBrand.GetUltTarget()
    local targets = {}
    local hitCount = {}
    local avgDist = {}
    for _, v in pairs(ObjectManager.Get("enemy", "heroes")) do
        if Player.Position:Distance(v) < Spells.R.Range and Utils.IsValidTarget(v) then
            table.insert(targets, v)
            hitCount[v] = 0
            avgDist[v] = 0
        end
    end

    if table.len(targets) == 1 then
        return targets[1], 1, 0
    end

    local bestTarget = nil
    for _, t in pairs(targets) do
        for _, v in pairs(ObjectManager.Get("enemy", "heroes")) do
            if t ~= v and Utils.IsValidTarget(v) then
                local dist = t.Position:Distance(v)
                if dist < Spells.R.EffectRadius then
                    hitCount[t] = hitCount[t] + 1
                    if hitCount[t] == 1 then
                        avgDist[t] = dist
                    else
                        avgDist[t] = (avgDist[t] + dist) / 2
                    end
                end
            end
        end
        if not bestTarget then
            bestTarget = t
        else
            if hitCount[bestTarget] < hitCount[t] then
                bestTarget = t
            end
        end
    end
    return bestTarget, hitCount[bestTarget], avgDist[bestTarget]
end

function WBBrand.Background()
    
    if not Menu.Get("Harass.Auto") then
        return false
    end

    if Player:GetSpellState(Spells.Q.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.GetTarget(Spells.Q.Range)
        if target then
            local qPred = Prediction.GetPredictedPosition(target, Spells.Q, Player.Position)
            if qPred then 
                if qPred.HitChance >= Menu.Get("Harass.HitChanceQ") then
                    qPred = qPred.CastPosition
                else 
                    qPred = nil
                end
            end
            if qPred and WBBrand.IsAblaze(target) then
                if Spell.Skillshot(Spells.Q):Cast(qPred) then
                    return true
                end
            end
        end
    end
    
    if Player.ManaPercent < Menu.Get("Harass.MinMana") then
        return false
    end

    if Player:GetSpellState(Spells.W.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.GetTarget(Spells.W.Range)
        if target then
            local wPred = target:FastPrediction(WBBrand.predWDelay())
            local inRange = Player.Position:Distance(wPred) < Spells.W.Range
            if inRange then
                if Spell.Targeted(Spells.W):Cast(wPred) then
                    return true
                end
            end
        end
    end
    if Player:GetSpellState(Spells.E.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.FindConflagTarget()
        if target then
            if Spell.Targeted(Spells.E):Cast(target) then
                return true
            end
        end
    end
end

function WBBrand.Harass()
    if Player:GetSpellState(Spells.Q.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.GetTarget(Spells.Q.Range)
        if target then
            local qPred = Prediction.GetPredictedPosition(target, Spells.Q, Player.Position)
            if qPred then 
                if qPred.HitChance >= Menu.Get("Harass.HitChanceQ") then
                    qPred = qPred.CastPosition
                else 
                    qPred = nil
                end
            end
            if qPred and WBBrand.IsAblaze(target) then
                if Spell.Skillshot(Spells.Q):Cast(qPred) then
                    return true
                end
            end
        end
    end

    if Player:GetSpellState(Spells.W.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.GetTarget(Spells.W.Range)
        if target then
            local wPred = target:FastPrediction(WBBrand.predWDelay())
            local inRange = Player.Position:Distance(wPred) < Spells.W.Range
            if inRange then
                if Spell.Targeted(Spells.W):Cast(wPred) then
                    return true
                end
            end
        end
    end
    if Player:GetSpellState(Spells.E.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.FindConflagTarget()
        if target then
            if Spell.Targeted(Spells.E):Cast(target) then
                return true
            end
        end
    end
end

function WBBrand.Combo()
    if Player:GetSpellState(Spells.W.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.GetTarget(Spells.W.Range)
        if target then
            local wPred = target:FastPrediction(WBBrand.predWDelay())
            local inRange = Player.Position:Distance(wPred) < Spells.W.Range
            if inRange then
                if Spell.Targeted(Spells.W):Cast(wPred) then
                    return true
                end
            end
        end
    end

    if Player:GetSpellState(Spells.Q.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.GetTarget(Spells.Q.Range)
        if target then
            local qPred = Prediction.GetPredictedPosition(target, Spells.Q, Player.Position)
            if qPred then 
                if qPred.HitChance >= Menu.Get("Combo.HitChanceQ") then
                    qPred = qPred.CastPosition
                else 
                    qPred = nil
                end
            end
            if qPred and WBBrand.IsAblaze(target) then
                if Spell.Skillshot(Spells.Q):Cast(qPred) then
                    return true
                end
            end
        end
    end

    if Player:GetSpellState(Spells.E.Slot) == Enums.SpellStates.Ready then
        local target = WBBrand.FindConflagTarget() or Orbwalker.GetTarget()
        if target then
            if Spell.Targeted(Spells.E):Cast(target) then
                return true
            end
        end
    end
    
    if Player:GetSpellState(Spells.R.Slot) == Enums.SpellStates.Ready then
        local allies = Utils.filter(ObjectManager.GetNearby("ally", "heroes"), function(ally)
            return Player.Position:Distance(ally) < Spells.R.Range and not ally.IsMe
        end)

        local target, n, d = WBBrand.GetUltTarget()
        if target then
            if n == 1 then
                local allyInDanger = false
                if table.len(allies) > 0 then
                    allyInDanger = allies[1].HealthPercent < 0.3
                end
                local RDMG = 300 * Spell.Targeted(Spells.R):GetLevel() + Player.TotalAP
                if target.Health - RDMG <= 0 or allyInDanger then
                    if Spell.Targeted(Spells.R):Cast(target) then
                        return true
                    end
                end
            elseif n > 1 and d < Spells.R.EffectRadius * 0.70 then
                if Spell.Targeted(Spells.R):Cast(target) then
                    return true
                end
            end
        end
    end
end

function WBBrand.Waveclear()
    local minions = ObjectManager.Get("enemy", "minions")
    local pos, n = Spell.Skillshot(Spells.W):GetBestCircularCastPos(minions)
    -- Cast W
    if Player:GetSpellState(Spells.W.Slot) == Enums.SpellStates.Ready then
        if n > 1 then
            if Spell.Targeted(Spells.W):Cast(pos) then
                return true
            end
        end
    end

    -- Cast E (Need to optimize)
    if Player:GetSpellState(Spells.E.Slot) == Enums.SpellStates.Ready then
        local minions = ObjectManager.Get("enemy", "minions")
        minions = Utils.filter(minions, function(m)
            return Player.Position:Distance(m) < Spells.E.Range and WBBrand.IsAblaze(m)
        end)
        local vec = Utils.map(minions, function(m)
            return m.Position
        end)
        local pos, n  = BestCoveringCircle(vec, Spells.E.EffectRadius)
        if n > 1 then
            sort(minions, function (a, b)
                return pos:Distance(a.AsAI) < pos:Distance(b.AsAI)
            end)
            if minions[1] then
                if Spell.Targeted(Spells.E):Cast(minions[1]) then
                    return true
                end
            end
        end
    end
end

function WBBrand.OnGapclose(source, dash)
    if not source.IsEnemy then return end
    if Player:GetSpellState(Spells.W.Slot) == Enums.SpellStates.Ready then
        if Player.Position:Distance(source) < Spells.W.Range then
            if Spell.Targeted(Spells.W):Cast(source) then
                return true
            end
        end
    end
    return false
end

function IsGameNotAvailable()
    return Game.IsChatOpen() or Game.IsMinimized() or Player.IsDead
end

function WBBrand.OnTick()
    if IsGameNotAvailable() then return end

    if WBBrand.AutoAttack(Orbwalker.GetTarget()) then
        return true
    end

    local ModeToExecute = WBBrand[Orbwalker.GetMode()]
    if ModeToExecute then
        ModeToExecute()
    else
        WBBrand.Background()
    end
end

function WBBrand.SetupMenu()
    Menu.RegisterMenu("WBBrand", "wbBrand", function()
        Menu.Text("Settings")
        Menu.Slider("PredDelayW", "Prediction [W] Delay", 0.55, 0.25, 0.891 + 0.25, 0.001)
        Menu.Slider("AllyDangerHP", "Ally in danger HP [R]", 0.3, 0, 1, 0.001)
        Menu.Separator()

        Menu.Text("Harass")
        Menu.Checkbox("Harass.Auto", "Auto Harass", true)
        Menu.Slider("Harass.MinMana", "Auto Harass Minimum Mana", 0.6, 0, 1, 0.001)
        Menu.Slider("Harass.HitChanceQ", "HitChance [Q]", 0.65, 0, 1, 0.001)

        Menu.Separator()
        Menu.Text("Combo")
        Menu.Slider("Combo.HitChanceQ", "HitChance [Q]", 0.60, 0, 1, 0.001)
    end)
end

WBBrand.SetupMenu()
for EventName, EventId in pairs(Enums.Events) do
    if WBBrand[EventName] then
        EventManager.RegisterCallback(EventId, WBBrand[EventName])
    end
end
return true
